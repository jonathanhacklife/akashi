# A.K.A.S.H.I

Este es un ergonómico teclado portátil diseñado y programado para uno de los proyectos de Mercadolibre. Capaz de agilizar, facilitar y unificar las tareas de sus proyectos.

# ¿Tiempo o eficacia?

Hoy en día se han intentado realizar distintos métodos para unirlos y desarrollar las tareas del proyecto con comodidad y precisión.

Muchos moderadores principiantes cuyo rendimiento es bajo e intermedio, se encuentran con dificultades al momento de ejecutar sus tareas diarias debido a la sobrecarga de combinaciones de teclado.

![](images/image1.png)

Estas mismas suelen causar confusión y pérdida de tiempo junto con el uso constante del mouse, reduciendo la eficacia en la carga de datos.

![](images/image2.png)

Para contrarrestar estas problematicas se creó el proyecto AKASHI

# ¿Que es el proyecto A.K.A.S.H.I.?
_**A**rbusta’s **K**eyboard. **A**lternative, **S**imple, **H**elpful and **I**nnovative._

![](images/AKASHILogo.png)

**AKASHI** es el nuevo y ergonómico teclado portátil diseñado y programado en base a las problemáticas encontradas al momento de trabajar en la plataforma. Capaz de agilizar, facilitar y unificar las tareas/acciones del proyecto MeLi-CATÁLOGO en algo mucho más personalizado tanto para el moderador experimentado como para el moderador iniciante.

Con AKASHI se pueden realizar hasta más de VEINTIÚN ACCIONES, ya sea para el Match (la herramienta de trabajo utilizada por los Arbusters) como para el navegador.

## Rango de optimización
Se hizo una prueba piloto utilizando un dispositivo mejorado con algunos moderadores, entre ellos principiantes, la cual demostró una mejora de productividad y rendimiento.

![](images/rangeOptimization.png)

# ¿Que opinan los Arbusters (empleados)?:
> "Esta bueno, es cómodo. Siempre me manejé con teclados pero me agrada que AKASHI sea más portatil" -Jackelina

> "Al ser más ergonómico, con el teclado convencional es incómodo y creo que si alguien empieza con éste teclado podría aumentar su productividad" -Sofía

> "Pensé que me iba a llevar más tiempo adaptarme, pero es muy sencillo de usar" -Anahí

# Otros detalles
Actualmente el alcance de los Sistemas Operativos son Windows 7, 7 Pro, 8, 10, 10 Pro y Linux.

## Componentes utilizados en este proyecto
### Hardware
- SparkFun Pushbutton switch 12mm × (20)	
- Jumper wires × (1)
- Microchip Technology ATmega328 × (1)
- Micro-USB a USB Cable × (1)

### Herramientas manuales y máquinas de fabricación.
- Soldador y estaño
